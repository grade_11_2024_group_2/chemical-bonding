# WELCOME TO OUR DOCUMENTATION

**"Happiness is homemade, and you're welcome to share ours."**

We are the Bonds Group, and we have created a robot. We hope you will have fun watching our robot.

# STEP 1: 
**Material that we are using:** 
1. Cardboard
2. Battery
3. Glue Gun
4. Popsicle sticks
5. Tooth pick stick
6. Moter
7. Connector(Jumper wire)


# STEP 2
**SKETCH PROCESS**
![](images/design.JPG)
![](images/Leg_design.jpg)
![](images/Gear.png)

We used cardboard for the body and wood to make the gears
# STEP 3
**USING LASER CUTTING**
![](images/Laser_cutting.JPG)

**Finished cutting**
![](images/Finish_cutting.jpg)

# STEP 4
**Working Process**
![](images/Design_process.jpg)
![](images/Working.JPG)
![](images/work_project.jpg)
<video controls src="images/motor.mp4" title="Title"></video>
<video controls src="images/working_body.mp4" title=""></video>

![](images/Mechanic.jpg)

# Reference:
[![Video Preview](https://i.ytimg.com/vi/Z7N0xCDVzIA/maxresdefault.jpg)](https://youtu.be/Z7N0xCDVzIA?si=whUFItQXmEQCr84O)
[![Video Preview](https://i.ytimg.com/vi/P0nFkex-y6A/maxresdefault.jpg)](https://youtu.be/P0nFkex-y6A?si=uFlPYim3plIWicKd)
[Simple Walking Robot Project](https://www.sciencebuddies.org/science-fair-projects/project-ideas/Robotics_p057/robotics/simple-walking-robot)

[Walking Robot Pinterest Board](https://www.pinterest.com/slmeardon/walking-robot/)



# Team Member:
- Choeyang Yeshey Dema
- Tshering Wangmo
- Ugyen Choden
- Deki Lhazom





