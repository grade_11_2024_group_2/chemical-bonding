[Home](./Index.md) | [Project](./Project_Idea.md)   |  [Team](./team.md)  |  [Terminologies](./terminologies.md)
# Project Idea
 **Using Gitlab:**

![](images/gitlab.png)

**Material needed:**
1. Plywood
2. Stainless steel wire
3. Beads
4. Disc magnets
5. Breadboard
6. Arduino Board
7. Jumper wire
8. Motor
9. Oled
10. Ultrasonic sensor
11. Hall effect sensor

# Components description:
- **Plywood**: It is to be used as a base to  support the chemical bonding model and a place where arduino is going to be embedded
- **Beads**: It will be acting as the electrons and thus running through the stainless steel wire through its inner circle (the hole).
-  **Stainless steel wire**: It is to be used to create a chemical bonding model where its soul function is to form a circular path that acts as a shell of the atom and hold the beats which will act as the electrons.
- **Dics magnet**:  It will be also used as the electrons are some parts.
-  **Arduino board**: It is a pocket-size object (also called as “microcontrollers”) that can be used to program and control circuits. So in this project it will be also used to run programs.
-  **Breadboard**: It is a tool used to build and test the electronic circuits. In this project breadboard is to be used to make quick electrical connections between the components.
-  **Jumper wire**: It is used to connect the components on a breadboard and connect components to the arduino board.
- **Motor**: It is a device that converts electrical energy into mechanical energy, generating motion. In this project it is to be used to spin the chemical bonding model on the plywood base.
-  **Oled**:  It is a display technology using organic compounds to emit light when an electric current passes through them. It's used in TVs, smartphones, and wearable devices for vibrant, energy-efficient screens. So in this project it is to be used to display messages related to the chemical bonding model.

-  **Ultrasonic sensor**: An ultrasonic sensor is a device that emits high-frequency sound waves (ultrasound) and then detects the waves that bounce back after hitting an object. In this project, we are using the sensor to sense something and make it work on its own.
-  **Hall effect sensor**: Hall-effect sensors are integrated circuits that transduce magnetic fields to electrical signals with accuracy, consistency, and reliability.

# STEP 1
**Sketch process**

**1**.**Base front view**
![](images/Base_front_view.jpg)
**2**.**Base top view**
![](images/Base_top_view.jpg)
**3**. **Chemical structure**
![](images/Chemical_structure.jpg)
**4**. **Laser cutting measurement**
![](images/Laser_cutting_measurement.jpg)
![](images/Laser_cutting.jpg)
**5**. **Whole top view**
![](images/Top_view.jpg)
**6**. **Whole model**
![](images/Whole_model.jpg)

# STEP 2
**Base Design:**


![](images/base_part.png)
![](images/base_another.png)

**Beads Design**


![](images/beads_design.png)


# STEP 3
**Using Laser cutting**

**Box Designed**
![](images/Box_designed.png)
**Completed**
![](images/Completed.jpg)
**Final Box** 
![](images/Box.jpg)

# STEP 4
# Circuit Design
![](images/Circuit_design.png)
We used a servo motor to represent a motor because there was no motor available while we were designing the circuit online.

# COMPLETE DESIGN
**BASE AND CIRCUIT DESIGN**
![](images/BASE_AND_CIRCUIT.jpg)

**VALENCE SHELL**
![](images/valence_shell.jpg)


[Home](./Index.md) | [Project](./Project_Idea.md)   |  [Team](./team.md)  |  [Terminologies](./terminologies.md)